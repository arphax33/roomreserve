package fr.epsi.b3.service;

import fr.epsi.b3.modele.Guest;
import fr.epsi.b3.modele.Reservation;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.TimeZone;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.parameter.Role;
import net.fortuna.ical4j.model.property.Attendee;
import net.fortuna.ical4j.model.property.DtEnd;
import net.fortuna.ical4j.model.property.DtStart;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.util.FixedUidGenerator;
import net.fortuna.ical4j.util.UidGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.SocketException;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class CalendarService {
    @Autowired
    private ReservationService reservationService;

    @Autowired
    private GuestService guestService;

    // Generate ICalendar Event from Reservation id in parameter
    @Transactional (readOnly = true)
    public String generateEvent(Integer id) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:ss:SS");
            net.fortuna.ical4j.model.Calendar iCalendar = new net.fortuna.ical4j.model.Calendar();
            Calendar startDate = new GregorianCalendar();
            Calendar endDate = new GregorianCalendar();
            List<Attendee> attendeeList = new ArrayList<>();

            // Getting the Reservation
            Reservation reservation = reservationService.getOneById(id);

            // Getting the Guest list
            List<Guest> guestList = guestService.getGuestsByReservation(reservation);

            // Getting the TimeZone
            TimeZoneRegistry tZRegistry = TimeZoneRegistryFactory.getInstance().createRegistry();
            TimeZone timeZone = tZRegistry.getTimeZone(startDate.getTimeZone().getID());
            VTimeZone vTimeZone = timeZone.getVTimeZone();

            // Getting the hours and date of the event
            simpleDateFormat.setTimeZone(startDate.getTimeZone());
            startDate.setTime(simpleDateFormat.parse(reservation.getDateDebut()));
            endDate.setTime(simpleDateFormat.parse(reservation.getDateFin()));

            // Generate VEvent identifier
            FixedUidGenerator uId = new FixedUidGenerator(id.toString());

            // Creating the event
            String eventName = reservation.getNom();
            DateTime start = new DateTime(startDate.getTime());
            DateTime end = new DateTime(endDate.getTime());
            VEvent event = new VEvent(start, end, eventName);

            // Getting the room
            Location location = new Location();
            location.setValue("Salle " + reservation.getRoom().getNumero());

            // Adding parameters in terms of iCalendar format
            event.getProperties().add(vTimeZone.getTimeZoneId());
            event.getProperties().add(uId.generateUid());
            event.getProperties().add(location);

            // Adding guests
            for (Guest guest: guestList) {
                Attendee attendee = new Attendee(URI.create("mailto:" + guest.getMail()));
                attendee.getParameters().add(Role.REQ_PARTICIPANT);
                event.getProperties().add(attendee);
            }

            // Adding event and return it to iCalendar format
            iCalendar.getComponents().add(event);
            return iCalendar.toString();
        }
        catch (ParseException | SocketException e) {
            e.printStackTrace();
        }

        return "error";
    }
}
