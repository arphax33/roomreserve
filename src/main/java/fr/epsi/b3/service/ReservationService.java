package fr.epsi.b3.service;

import fr.epsi.b3.dao.ReservationDao;
import fr.epsi.b3.modele.Reservation;
import fr.epsi.b3.modele.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReservationService {
    @Autowired
    private ReservationDao reservationDao;

    public Reservation getOneById(Integer id) {
        return reservationDao.findOneById(id);
    }
}
