package fr.epsi.b3.service;

import fr.epsi.b3.modele.Room;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class PdfService {
    private static String QRCODE_TEMPLATE = "templates/qrcode.jasper";

    // Generate a PDF with QrCode leading to a form to reserve it
    public byte[] generateRoomPdf(Integer id, String uri, String number) {
        // Setting parameters to send to the template
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("URL", uri);
        parameters.put("ROOM_NUMBER", number);

        // Getting the template of PDF
        InputStream template = getClass().getClassLoader().getResourceAsStream(QRCODE_TEMPLATE);

        byte[] pdf = exportToPdf(template, parameters);

        try {
            template.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        finally {
            return pdf;
        }
    }

    // Fill the template with parameters and export to PDF as bytes array
    private byte[] exportToPdf(InputStream template, HashMap<String, Object> parameters) {
        try {
            JasperPrint filledReport = JasperFillManager.fillReport(template, parameters, new JREmptyDataSource());
            return JasperExportManager.exportReportToPdf(filledReport);
        }
        catch (JRException e) {
            e.printStackTrace();
        }

        // Return a void bytes array if error occur
        return new byte[0];
    }
}
