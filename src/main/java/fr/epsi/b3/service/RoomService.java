package fr.epsi.b3.service;

import fr.epsi.b3.dao.RoomDao;
import fr.epsi.b3.modele.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class RoomService {
    @Autowired
    private RoomDao roomDao;

    @Transactional (readOnly = true)
    public List<Room> getAll() {
        return roomDao.findAll();
    }

    @Transactional (readOnly = true)
    public Room getOneById(Integer id) {
        return roomDao.findOneById(id);
    }
}
