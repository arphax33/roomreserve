package fr.epsi.b3.service;

import fr.epsi.b3.dao.GuestDao;
import fr.epsi.b3.modele.Guest;
import fr.epsi.b3.modele.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GuestService {
    @Autowired
    private GuestDao guestDao;

    @Transactional(readOnly = true)
    public List<Guest> getGuestsByReservation(Reservation reservation) {
        return guestDao.findGuestsByReservation(reservation);
    }
}
