package fr.epsi.b3.dao;

import java.util.List;

import fr.epsi.b3.modele.Room;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class RoomDao {
    @PersistenceContext
    private EntityManager entityManager;

    public List<Room> findAll() {
        return entityManager.createQuery("SELECT r FROM Room r", Room.class)
                .getResultList();
    }

    public Room findOneById(Integer id) {
        return entityManager.createQuery("SELECT r FROM Room r WHERE r.id=:id", Room.class)
                .setParameter("id", id)
                .getSingleResult();
    }
}
