package fr.epsi.b3.dao;

import fr.epsi.b3.modele.Reservation;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class ReservationDao {
    @PersistenceContext
    private EntityManager entityManager;

    public Reservation findOneById(Integer id){
        return entityManager.createQuery("SELECT r FROM Reservation r WHERE r.id=:id", Reservation.class)
                .setParameter("id", id)
                .getSingleResult();
    }
}
