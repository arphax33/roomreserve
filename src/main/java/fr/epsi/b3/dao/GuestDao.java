package fr.epsi.b3.dao;

import fr.epsi.b3.modele.Guest;
import fr.epsi.b3.modele.Reservation;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class GuestDao {
    @PersistenceContext
    private EntityManager entityManager;

    public List<Guest> findGuestsByReservation (Reservation reservation) {
        return entityManager.createQuery("SELECT g FROM Guest g WHERE g.reservation.id=:reservation_id", Guest.class)
                .setParameter("reservation_id", reservation.getId())
                .getResultList();
    }
}
