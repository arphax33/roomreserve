package fr.epsi.b3.controller;

import fr.epsi.b3.modele.Room;
import fr.epsi.b3.service.PdfService;
import fr.epsi.b3.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.GregorianCalendar;

@Controller
public class RoomController {
    @Autowired
    private RoomService roomService;

    @Autowired
    private PdfService pdfService;

    // Generate a PDF with QrCode leading to a form to reserve it from the id in path
    @GetMapping(path={"/{id}/room/pdf"}, produces = "application/pdf")
    @ResponseBody
    public byte[] pdf(@PathVariable(value = "id") String id, UriComponentsBuilder builder) {
        Room room = roomService.getOneById(Integer.parseInt(id));
        String uri = builder.path("/" + room.getId().toString() + "/room/reserve").toUriString();
        return pdfService.generateRoomPdf(Integer.parseInt(id), uri, room.getNumero());
    }

    // Display the rooms list with buttons to generate the QRCode for it
    @GetMapping({"/room"})
    public String index(HttpServletRequest request, Model model) {
        model.addAttribute(roomService.getAll());
        return "roomList";
    }
}
