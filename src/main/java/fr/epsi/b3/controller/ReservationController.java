package fr.epsi.b3.controller;

import fr.epsi.b3.service.CalendarService;
import fr.epsi.b3.service.PdfService;
import net.fortuna.ical4j.model.Calendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

@Controller
public class ReservationController {
    @Autowired
    private CalendarService calendarService;

     // Lead to a form permitting to reserve a room from the id in path
    @GetMapping({"/{id}/reservation/room"})
    public String reservationForm(@PathVariable(value = "id") String id) {
        return "reservation";
    }

    @GetMapping(value = {"/{id}/reservation/event"}, produces = "text/calendar")
    public ResponseEntity reservationEvent(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok().body(calendarService.generateEvent(Integer.parseInt(id)));
    }
}
