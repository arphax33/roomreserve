drop table if exists User;
drop table if exists Room;
drop table if exists Reservation;
drop table if exists Guest;

create table User (
  id int primary key auto_increment,
  nom varchar(50) not null,
  prenom varchar(50) not null,
  mail varchar(50) not null unique,
  password varchar(50) not null
) engine = InnoDB;

create table Room (
  id int primary key auto_increment,
  numero varchar(20) not null unique
) engine = InnoDB;

create table Reservation (
  id int primary key auto_increment,
  dateDebut datetime not null,
  dateFin datetime not null,
  nom varchar(50) not null,
  description varchar(255) not null,
  User_id int, 
  Room_id int,
  foreign key fk_user(User_id) references User(id),
  foreign key fk_room(Room_id) references Room(id)  
) engine = InnoDB;

create table Guest (
  id int primary key auto_increment,
  mail varchar(50) not null,
  Reservation_id int,
  foreign key fk_reservation(Reservation_id) references Reservation(id)
) engine = InnoDB;

insert into Room (numero)
values
("MyDIL"),
("105"),
("106"),
("201"),
("204"),
("207");


insert into User(nom, prenom, mail, password)
values
("Rodrigues", "Matthieu", "matthieu.rodrigues@epsi.fr", "p@$$w0rd"),
("Sylva", "Laurence", "laurence.dasylva@epsi.fr", "m0tdep@$$e");


insert into Reservation(dateDebut, dateFin, nom, description, User_id, Room_id)
values
('19-12-10 14:00:00', '19-12-10 16:00:00', "Réunion projet" ,"ceci est une description", 1, 3);

insert into Guest(mail, Reservation_id)
values
("lilian.berna@epsi.fr", 1)
