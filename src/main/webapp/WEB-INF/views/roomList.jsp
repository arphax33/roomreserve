<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: At0m
  Date: 11/12/2019
  Time: 20:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>RoomReserve - Liste des salles</title>
</head>
<body>
<table>
    <thead>
        <th>Numero</th>
        <th>Actions</th>
    </thead>
    <tbody>
    <c:forEach items="${roomList}" var="room" varStatus="status">
        <tr>
            <td><c:out value="${room.numero}"/></td>
            <td><a href="<c:url value="/${room.id}/room/pdf"/>">QRCode</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
